import UIKit
import Foundation
import PlaygroundSupport

// Basic program that calculates a statement of a customer's charges at a car rental store.
//
// A customer can have multiple "Rental"s and a "Rental" has one "Car"
// As an ASCII class diagram:
//          Customer 1 ----> * Rental
//          Rental   * ----> 1 Car
//
// The charges depend on how long the car is rented and the type of the car (economy, muscle or supercar)
//
// The program also calculates frequent renter points.
//
//
// Refactor this class how you would see fit.
//
// The actual code is not that important, as much as its structure. You can even use "magic" functions (e.g. foo.sort()) if you want

enum PriceCode {
    case economy
    case supercar
    case muscle
    
    var defaultAmount: Double {
        switch self {
        case .economy:
            return 80
        case .supercar:
            return 0
        case .muscle:
            return 200
        }
    }
}

class Car {
    let title: String
    let priceCode: PriceCode
    
    init(title: String, priceCode: PriceCode) {
        self.title = title
        self.priceCode = priceCode
    }
}

class Rental {
    let car : Car
    private (set) var daysRented: Int
    
    init(car: Car, daysRented: Int) {
        self.car = car
        self.daysRented = daysRented
    }
    
    private func defaultAmount() -> Double {
        return car.priceCode.defaultAmount
    }
    
    private func additionalAmount() -> Double {
        var amount: Double = 0
        switch car.priceCode {
        case .economy:
            if (daysRented > 2) {
                amount = (Double(daysRented) - 2) * 30.0
            }
        case .supercar:
            amount = Double(daysRented) * 200.0
        case .muscle:
            if (daysRented > 3) {
                amount = (Double(daysRented) - 3) * 50.0
            }
        }
        return amount
    }
    
    func amount() -> Double {
        return defaultAmount() + additionalAmount()
    }
    
    func renterPoints() -> Int {
        // add bonus for a two day new release rental
        if ((car.priceCode == .supercar) && daysRented > 1) {
            return 2
        } else {
            return 1
        }
    }
    
    func figures() -> String {
        return "\t" + car.title + "\t" + String(amount())
    }
}

class Customer {
    private let name: String
    private var rentals = [Rental]()
    
    init(name: String) {
        self.name = name
    }
    
    func add(rental: Rental) {
        rentals.append(rental)
    }
    
    func billingStatement() -> String {
        
        var totalAmount: Double = 0
        var frequentRenterPoints = 0
        
        var result = "Rental Record for " + name + "\n"
        
        rentals.forEach { rental in
            // add frequent renter points
            frequentRenterPoints += rental.renterPoints()
            //show figures for this rental
            result += rental.figures() + "\n"
            totalAmount += rental.amount()
        }
        //add footer lines
        result += "Final rental payment owed " + String(totalAmount) + "\n"
        result += "You received an additional " + String(frequentRenterPoints) + " frequent customer points"
        return result
    }
}

let rental1 = Rental(car: Car(title: "Mustang", priceCode: .muscle), daysRented: 5)
let rental2 = Rental(car: Car(title: "Lambo", priceCode: .supercar), daysRented: 20)
let customer = Customer(name: "Liviu")
customer.add(rental: rental1)
customer.add(rental: rental2)


print(customer.billingStatement())


let test = "@My Very First Commit@ $by AdiR$ #s. f.# (#Med.#) This fixes most important bugs #2020-10-10# @Weather App@"

class Collector {
    private (set) var separator: Character? = nil
    private (set) var result: AttributedString = AttributedString("")
    private (set) var currentString: String = ""

    var hasCustomStyleSeparator: Bool {
        return separator == "@" || separator == "$"
    }

    var hasDeletionSeparator: Bool {
        return separator == "#"
    }

    init(separator: Character?, result: AttributedString, currentString: String) {
        self.separator = separator
        self.result = result
        self.currentString = currentString
    }

    private func resetAndAppendNormalStyle(character: Character) {
        separator = character
        if currentString.count > 0 {
            result.append(AttributedString(currentString, attributes: AttributeContainer([.font: UIFont.systemFont(ofSize: 17)])))
            currentString = ""
        }
    }

    private func resetAndAppendBoldStyle() {
        result.append(AttributedString(currentString, attributes: AttributeContainer([.font: UIFont.boldSystemFont(ofSize: 17)])))
        currentString = ""
        separator = nil
    }

    private func resetAndAppendItalicStyle() {
        result.append(AttributedString(currentString, attributes: AttributeContainer([.font: UIFont.italicSystemFont(ofSize: 17)])))
        currentString = ""
        separator = nil
    }

    private func resetAndAppendCustomStyle() {
        if separator == "@" {
            resetAndAppendBoldStyle()
        } else if separator == "$" {
            resetAndAppendItalicStyle()
        }
    }

    func resetForEmptySeparator(character: Character) {
        if character == "@" || character == "$" {
            resetAndAppendNormalStyle(character: character)
        } else if character == "#" {
            separator = "#"
        } else {
            currentString.append(character)
        }
    }

    func resetForCustomSeparator(character: Character) {
        if character != separator {
            currentString.append(character)
        } else {
            resetAndAppendCustomStyle()
        }
    }

    func resetForDeletionSeparator(character: Character) {
        if character == "#" {
            separator = nil
        }
    }

    func append(character: Character) {
        currentString.append(character)
    }
}
let collector = Collector(separator: nil, result: AttributedString(""), currentString: "")

test.forEach { character in
    if collector.separator == nil {
        collector.resetForEmptySeparator(character: character)
    } else if collector.hasCustomStyleSeparator {
        collector.resetForCustomSeparator(character: character)
    } else if collector.hasDeletionSeparator {
        collector.resetForDeletionSeparator(character: character)
    } else {
        collector.append(character: character)
    }
}

class TestViewController: UIViewController {
    let label = UILabel(frame: CGRect(x: 200, y: 200, width: 300, height: 500))


    override func viewDidLoad() {
        label.numberOfLines = 0
        self.view.addSubview(label)
    }
}

let vc = TestViewController()
vc.label.attributedText = NSAttributedString(collector.result)
PlaygroundPage.current.liveView = vc
