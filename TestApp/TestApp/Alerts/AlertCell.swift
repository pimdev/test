//
//  AlertCell.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 04.04.2022.
//

import UIKit

class AlertCell: UICollectionViewCell {
    let nameLabel: UILabel
    let startDateLabel: UILabel
    let endDateLabel: UILabel
    let sourceLabel: UILabel
    let imageView: UIImageView
    
    override init(frame: CGRect) {
        nameLabel = UILabel()
        nameLabel.textColor = .white
        startDateLabel = UILabel()
        startDateLabel.textColor = .white
        endDateLabel = UILabel()
        endDateLabel.textColor = .white
        sourceLabel = UILabel()
        sourceLabel.textColor = .white
        imageView = UIImageView()
        super.init(frame: .zero)
        contentView.addSubview(nameLabel)
        contentView.addSubview(startDateLabel)
        contentView.addSubview(endDateLabel)
        contentView.addSubview(sourceLabel)
        contentView.addSubview(imageView)
        configureViews()
    }
    
    func configureViews() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8).isActive = true
        nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8).isActive = true
        startDateLabel.translatesAutoresizingMaskIntoConstraints = false
        startDateLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        startDateLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4).isActive = true
        startDateLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        endDateLabel.translatesAutoresizingMaskIntoConstraints = false
        endDateLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        endDateLabel.topAnchor.constraint(equalTo: startDateLabel.bottomAnchor, constant: 4).isActive = true
        endDateLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        sourceLabel.translatesAutoresizingMaskIntoConstraints = false
        sourceLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        sourceLabel.topAnchor.constraint(equalTo: endDateLabel.bottomAnchor, constant: 4).isActive = true
        sourceLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        sourceLabel.numberOfLines = 0
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: sourceLabel.bottomAnchor, constant: 4).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

