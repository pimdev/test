//
//  NetworkModel.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 04.04.2022.
//

import Foundation

struct WeatherInfo: Decodable {
    private enum CodingKeys: String, CodingKey {
        case context = "@context", type, features
    }
    let context: [ContextInfo]
    let type: String
    let features: [FeatureInfo]
}

struct FeatureInfo: Decodable {
    let id: String
    let type: String
    let properties: PropertyInfo
}

struct PropertyInfo: Decodable {
    let event: String
    let effective: String
    let expires: String
    let senderName: String
    let severity: String
    let certainty: String
    let urgency: String
    let description: String
    let instruction: String?
    let affectedZones: [String]
}

enum ContextInfo: Decodable {
    case string(String)
    case version(VersionInfo)
    case none
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let string = try? container.decode(String.self) {
            self = .string(string)
        } else if let versionInfo = try? container.decode(VersionInfo.self) {
            self = .version(versionInfo)
        } else {
            self = .none
        }
    }
}

struct VersionInfo: Decodable {
    private enum CodingKeys: String, CodingKey {
        case version = "@version", wx
        case vocab = "@vocab"
    }
    let version: String
    let wx: String
    let vocab: String
}

struct AffectedZoneProperty: Decodable {
    let name: String
    let radarStation: String?
}

struct AffectedZone: Decodable {
    let properties: AffectedZoneProperty
}
