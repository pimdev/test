//
//  API.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 04.04.2022.
//

import Foundation
import Combine

protocol AlertsManagerDelegate: AnyObject {
    func didUpdateData(data: [AlertCellInfo])
    func didLoadData(data: [AffectedZone], for item: AlertCellInfo)
}

enum ServerError: Error {
    case bad
}

class AlertsManager {
    private var cancellables: [AnyCancellable] = []

    weak var delegate: AlertsManagerDelegate?
    
    private func getAlertInfoDataPublisher() -> AnyPublisher<[AlertCellInfo], Error>? {
        return getWeatherDataPublisher()?.tryMap({ weatherInfo in
            weatherInfo.features.map {
                return AlertCellInfo(name: $0.properties.event,
                                     startDate: $0.properties.effective,
                                     endDate: $0.properties.expires,
                                     source: $0.properties.senderName,
                                     severity: $0.properties.severity,
                                     certainty: $0.properties.certainty,
                                     urgency: $0.properties.urgency,
                                     description: $0.properties.description,
                                     instruction: $0.properties.instruction,
                                     affectedZones: $0.properties.affectedZones,
                                     id: UUID().uuidString) }
        }).eraseToAnyPublisher()
    }
    
    private func getWeatherDataPublisher() -> AnyPublisher<WeatherInfo, Error>? {
        var result: AnyPublisher<WeatherInfo, Error>?
        if let url = URL(string: "https://api.weather.gov/alerts/active?status=actual&message_type=alert") {
            result = URLSession.shared.dataTaskPublisher(for: url)
                .receive(on: DispatchQueue.main)
                .tryMap { data, response -> Data in
                    guard let httpResponse = response as? HTTPURLResponse,
                          200..<300 ~= httpResponse.statusCode else {
                        throw ServerError.bad
                    }
                    return data
                }
                .map { $0 }
                .decode(type: WeatherInfo.self, decoder: JSONDecoder())
                .eraseToAnyPublisher()
        }
        return result
    }
    
    private func getAffectedZonesDataPublisher(urlString: String) -> AnyPublisher<AffectedZone, Error>? {
        var result: AnyPublisher<AffectedZone, Error>?
        if let url = URL(string: urlString) {
            result = URLSession.shared.dataTaskPublisher(for: url)
                .receive(on: DispatchQueue.main)
                .tryMap { data, response -> Data in
                    guard let httpResponse = response as? HTTPURLResponse,
                          200..<300 ~= httpResponse.statusCode else {
                        throw ServerError.bad
                    }
                    return data
                }
                .map { $0 }
                .decode(type: AffectedZone.self, decoder: JSONDecoder())
                .eraseToAnyPublisher()
        }
        return result
    }
    
    func loadDetails(for item: AlertCellInfo) {
        let publishers = item.affectedZones.compactMap { getAffectedZonesDataPublisher(urlString: $0) }
        let cancellable = Publishers.MergeMany(publishers).collect().sink(receiveCompletion: { completion in
            print(completion)
        }, receiveValue: { output in
            self.delegate?.didLoadData(data: output, for: item)
        })
        cancellables.append(cancellable)
    }
    
    func loadData() {
        if let publisher = getAlertInfoDataPublisher() {
            let cancellable = publisher.sink { completion in
                print(completion)
            } receiveValue: { [weak self] info in
                self?.delegate?.didUpdateData(data: info)
            }
            cancellables.append(cancellable)
        }
    }
}
