//
//  AlertViewController.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 04.04.2022.
//

import UIKit
import Combine

class AlertViewController: BaseCollectionViewController {
    
    private var viewModel: AlertsManager = AlertsManager()
    private var data: [AlertCellInfo] = []
    private let imageQueue = DispatchQueue(label: "imageQueue")
    private var cache: [Int: UIImage] = [:]
    
    private lazy var dataSource = UICollectionViewDiffableDataSource<AlertSection, AlertCellInfo>(collectionView: collectionView) { (collectionView, indexPath, info) -> UICollectionViewCell? in
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlertCell", for: indexPath) as! AlertCell
        print(info.startDate)
        cell.nameLabel.text = info.name
        cell.startDateLabel.text = info.startDate
        cell.endDateLabel.text = info.endDate

        cell.sourceLabel.text = info.source
        cell.imageView.image = self.image(at: indexPath.item, for: cell)
        return cell
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        collectionView.register(AlertCell.self, forCellWithReuseIdentifier: "AlertCell")
        collectionView.delegate = self
        viewModel.delegate = self
        viewModel.loadData()
    }
    
    private func applySnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot<AlertSection, AlertCellInfo>()
        snapshot.appendSections([.main])
        
        snapshot.appendItems(data, toSection: .main)
        
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    private func image(at index: Int, for cell: AlertCell?) -> UIImage? {
        if let image = cache[index] {
            return image
        } else {
            self.imageQueue.async {
                if let data = try? Data(contentsOf: URL(string: "https://picsum.photos/1000")!), let image = UIImage(data: data) {
                    self.cache[index] = image
                    self.data[index].image = image
                    DispatchQueue.main.async {
                        let cellIsVisible = self.collectionView.visibleCells.contains { visibleCell in
                            visibleCell == cell
                        }
                        if cellIsVisible {
                            cell?.imageView.image = image
                        }
                    }
                }
            }
            return nil
        }
    }
}

extension AlertViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.loadDetails(for: data[indexPath.item])
    }
}

extension AlertViewController: AlertsManagerDelegate {
    func didUpdateData(data: [AlertCellInfo]) {
        self.data = data
        applySnapshot()
    }
    
    func didLoadData(data: [AffectedZone], for item: AlertCellInfo) {
        let detailsViewController = DetailsViewController(info: DetailsInfo(affectedZones: data, info: item))
        self.present(detailsViewController, animated: true)
    }
}
