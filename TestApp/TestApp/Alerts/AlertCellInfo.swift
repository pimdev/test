//
//  UIModel.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 04.04.2022.
//

import Foundation
import UIKit

let dateFormatter = ISO8601DateFormatter()

struct AlertCellInfo: Hashable {
    let name: String
    let startDate: String
    let endDate: String
    let source: String
    let severity: String
    let certainty: String
    let urgency: String
    let description: String
    let instruction: String?
    let affectedZones: [String]

    // generated
    let id: String

    // assigned
    var image: UIImage?
    
    var period: String {
        if let start = dateFormatter.date(from: startDate),
           let end = dateFormatter.date(from: endDate) {
            return "\(start.shortForm()) - \(end.shortForm())"
        } else {
            return "\(startDate) - \(endDate)"
        }
    }
}

enum AlertSection {
    case main
}

extension Date {
    func shortForm() -> String {
        formatted(date:FormatStyle.DateStyle.abbreviated,
                  time: FormatStyle.TimeStyle.shortened)
    }
}
