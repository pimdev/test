//
//  ExtendedDetailsViewController.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 06.04.2022.
//

import UIKit

class ExtendedDetailsViewController: BaseCollectionViewController {
    
    private var data: [ExtendedDetailsInfo] = []
    private var instructionLabelExpanded: Bool = false
    private var descriptionLabelExpanded: Bool = false
    
    private lazy var dataSource = UICollectionViewDiffableDataSource<AlertSection, ExtendedDetailsInfo>(collectionView: collectionView) { (collectionView, indexPath, info) -> UICollectionViewCell? in
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailsAlertCell", for: indexPath) as! DetailsAlertCell

        cell.periodLabel.text = "Period: \(info.period)"
        cell.severityLabel.text = "Severity: \(info.severity)"
        cell.certaintyLabel.text = "Certainty: \(info.certainty)"
        cell.urgencyLabel.text = "Urgency: \(info.urgency)"
        cell.sourceLabel.text = "Source: \(info.source)"
        cell.descriptionLabel.text = "Description: \(info.description)"
        cell.affectedZonesLabel.text = "Affected zones: \(info.affectedZones)"
        cell.instructionLabel.text = "Instruction: \(info.instruction ?? "-")"
        cell.descriptionLabel.numberOfLines = 2
        cell.instructionLabel.numberOfLines = 2
        cell.affectedZonesLabel.numberOfLines = 0
        cell.instructionLabel.isUserInteractionEnabled = true
        cell.instructionLabel.gestureRecognizers = [UITapGestureRecognizer(target: self, action: #selector(self.didTapInstructionLabel))]
        cell.descriptionLabel.isUserInteractionEnabled = true
        cell.descriptionLabel.gestureRecognizers = [UITapGestureRecognizer(target: self, action: #selector(self.didTapDescriptionLabel))]
        return cell
    }
    
    @objc
    func didTapInstructionLabel() {
        instructionLabelExpanded = !instructionLabelExpanded
        if let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as? DetailsAlertCell {
            cell.instructionLabel.numberOfLines = instructionLabelExpanded ? 0 : 2
        }
        applySnapshot()
    }
    
    @objc
    func didTapDescriptionLabel() {
        descriptionLabelExpanded = !descriptionLabelExpanded
        if let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as? DetailsAlertCell {
            cell.descriptionLabel.numberOfLines = descriptionLabelExpanded ? 0 : 2
        }
        applySnapshot()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(DetailsAlertCell.self, forCellWithReuseIdentifier: "DetailsAlertCell")
    }
    
    private func applySnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot<AlertSection, ExtendedDetailsInfo>()
        snapshot.appendSections([.main])
        
        snapshot.appendItems(self.data, toSection: .main)
        
        dataSource.apply(snapshot, animatingDifferences: true)
    }

    func loadData(_ data: [ExtendedDetailsInfo]) {
        self.data = data
        applySnapshot()
    }
}
