//
//  DetailsViewController.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 05.04.2022.
//

import UIKit

class DetailsViewController: UIViewController {
    let imageView: UIImageView = UIImageView()
    let info: DetailsInfo
    let infoViewController = ExtendedDetailsViewController()

    init(info: DetailsInfo) {
        self.info = info
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        view.addSubview(imageView)
        addChild(infoViewController)
        view.addSubview(infoViewController.view)
        
        configureViews()
        let info = self.info.info
        imageView.image = info.image
        
        let affectedZonesNames = self.info.affectedZones.map { $0.properties }.reduce("") { partialResult, next in
            var result = partialResult
            if !result.isEmpty { result.append(", ")}
            
            result.append(next.name)
            if (next.radarStation != nil)
            {
                result.append("R")
            }
            
            return result
        }
        
        infoViewController.loadData([ExtendedDetailsInfo(period: info.period, severity: info.severity, certainty: info.certainty, urgency: info.urgency, source: info.source, description: info.description, affectedZones: affectedZonesNames, instruction: info.instruction, id: UUID().uuidString)])
    }
    
    func configureViews() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.contentMode = .scaleAspectFit
        
        infoViewController.view.translatesAutoresizingMaskIntoConstraints = false
        infoViewController.view.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 8).isActive = true
        infoViewController.view.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8).isActive = true
        infoViewController.view.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8).isActive = true
        infoViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8).isActive = true
    }
}
