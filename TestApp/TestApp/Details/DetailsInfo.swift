//
//  DetailsInfo.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 06.04.2022.
//

import UIKit

struct ExtendedDetailsInfo: Hashable {
    let period: String
    let severity: String
    let certainty: String
    let urgency: String
    let source: String
    let description: String
    let affectedZones: String
    let instruction: String?
    let id: String
}

struct DetailsInfo {
    let affectedZones: [AffectedZone]
    let info: AlertCellInfo
}
