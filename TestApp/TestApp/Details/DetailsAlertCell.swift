//
//  DetailsAlertCell.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 06.04.2022.
//

import UIKit

class DetailsAlertCell: UICollectionViewCell {
    let periodLabel: UILabel
    let severityLabel: UILabel
    let certaintyLabel: UILabel
    let urgencyLabel: UILabel
    let sourceLabel: UILabel
    let descriptionLabel: UILabel
    let affectedZonesLabel: UILabel
    let instructionLabel: UILabel
    
    override init(frame: CGRect) {
        periodLabel = UILabel()
        periodLabel.textColor = .white
        severityLabel = UILabel()
        severityLabel.textColor = .white
        certaintyLabel = UILabel()
        certaintyLabel.textColor = .white
        urgencyLabel = UILabel()
        urgencyLabel.textColor = .white
        sourceLabel = UILabel()
        sourceLabel.textColor = .white
        descriptionLabel = UILabel()
        descriptionLabel.textColor = .white
        affectedZonesLabel = UILabel()
        affectedZonesLabel.textColor = .white
        instructionLabel = UILabel()
        instructionLabel.textColor = .white
        super.init(frame: .zero)
        contentView.addSubview(periodLabel)
        contentView.addSubview(severityLabel)
        contentView.addSubview(certaintyLabel)
        contentView.addSubview(urgencyLabel)
        contentView.addSubview(sourceLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(affectedZonesLabel)
        contentView.addSubview(instructionLabel)
        configureViews()
    }
    
    func configureViews() {
        let padding: CGFloat = 8
        periodLabel.translatesAutoresizingMaskIntoConstraints = false
        periodLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: padding).isActive = true
        periodLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: padding).isActive = true
        periodLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -padding).isActive = true
        severityLabel.translatesAutoresizingMaskIntoConstraints = false
        severityLabel.leftAnchor.constraint(equalTo: periodLabel.leftAnchor).isActive = true
        severityLabel.topAnchor.constraint(equalTo: periodLabel.bottomAnchor, constant: padding/2).isActive = true
        severityLabel.rightAnchor.constraint(equalTo: periodLabel.rightAnchor).isActive = true
        certaintyLabel.translatesAutoresizingMaskIntoConstraints = false
        certaintyLabel.leftAnchor.constraint(equalTo: periodLabel.leftAnchor).isActive = true
        certaintyLabel.topAnchor.constraint(equalTo: severityLabel.bottomAnchor, constant: padding/2).isActive = true
        certaintyLabel.rightAnchor.constraint(equalTo: periodLabel.rightAnchor).isActive = true
        urgencyLabel.translatesAutoresizingMaskIntoConstraints = false
        urgencyLabel.leftAnchor.constraint(equalTo: periodLabel.leftAnchor).isActive = true
        urgencyLabel.topAnchor.constraint(equalTo: certaintyLabel.bottomAnchor, constant: padding/2).isActive = true
        urgencyLabel.rightAnchor.constraint(equalTo: periodLabel.rightAnchor).isActive = true
        sourceLabel.translatesAutoresizingMaskIntoConstraints = false
        sourceLabel.leftAnchor.constraint(equalTo: periodLabel.leftAnchor).isActive = true
        sourceLabel.topAnchor.constraint(equalTo: urgencyLabel.bottomAnchor, constant: padding/2).isActive = true
        sourceLabel.rightAnchor.constraint(equalTo: periodLabel.rightAnchor).isActive = true
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.leftAnchor.constraint(equalTo: periodLabel.leftAnchor).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: sourceLabel.bottomAnchor, constant: padding/2).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: periodLabel.rightAnchor).isActive = true
        affectedZonesLabel.translatesAutoresizingMaskIntoConstraints = false
        affectedZonesLabel.leftAnchor.constraint(equalTo: periodLabel.leftAnchor).isActive = true
        affectedZonesLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: padding/2).isActive = true
        affectedZonesLabel.rightAnchor.constraint(equalTo: periodLabel.rightAnchor).isActive = true
        instructionLabel.translatesAutoresizingMaskIntoConstraints = false
        instructionLabel.leftAnchor.constraint(equalTo: periodLabel.leftAnchor).isActive = true
        instructionLabel.topAnchor.constraint(equalTo: affectedZonesLabel.bottomAnchor, constant: padding/2).isActive = true
        instructionLabel.rightAnchor.constraint(equalTo: periodLabel.rightAnchor).isActive = true
        instructionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -padding).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
