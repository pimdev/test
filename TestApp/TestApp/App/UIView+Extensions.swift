//
//  UIView+Extensions.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 05.04.2022.
//

import UIKit

extension UIView {
    func activateMatchParentConstraints(in view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
}
