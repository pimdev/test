//
//  BaseCollectionViewController.swift
//  TestApp
//
//  Created by Mihail-Ioan Popa on 05.04.2022.
//

import UIKit

class BaseCollectionViewController: UIViewController {
    var collectionView: UICollectionView!

    var itemSize: Float = 220
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
    }
    
    func layout() -> UICollectionViewCompositionalLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .estimated(100))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .estimated(100))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize,
                                                       subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
    
    func configureCollectionView() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout())
        collectionView.backgroundColor = .black
        collectionView.keyboardDismissMode = .onDrag
        view.addSubview(collectionView)
        collectionView.activateMatchParentConstraints(in: view)
    }
}
